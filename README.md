# Projektarbeit - Bikeculator

## Model

- app_model_v2.zip: Fertig trainiertes Modell, welches im Tensorflow Lite Format exportiert wurde, welches optimiert ist um auf mobilen Endgeräten genutzt zu werden (https://www.tensorflow.org/lite/guide)

- images.zip: Annotierte reale und synthetische Fahrradbilder welche zum Training des Models genutzt wurden

## App

- Bikeculator.zip: Exportiertes Android-Studi Projekt der Bikeculator App